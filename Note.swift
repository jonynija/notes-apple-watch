//
//  Note.swift
//  NotesApp WatchKit Extension
//
//  Created by Jonathan Ixcayau on 27/06/22.
//

import Foundation

struct Note: Codable, Identifiable, Equatable {
    var id: UUID
    
    var title: String
    var creationDate: String
    
    
    init(title: String){
        self.id = UUID()
        self.title = title
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .medium
        
        self.creationDate = dateFormatter.string(from: date)
    }
}
