//
//  NotesAppApp.swift
//  NotesApp WatchKit Extension
//
//  Created by Jonathan Ixcayau on 27/06/22.
//

import SwiftUI

@main
struct NotesAppApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }
    }
}
