//
//  ContentView.swift
//  NotesApp WatchKit Extension
//
//  Created by Jonathan Ixcayau on 27/06/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack{
            NavigationLink(
                "Agregar nota",
                destination: AddNote()
            )
            
            NavigationLink(
                "Ver notas",
                destination: ListNote()
            )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
