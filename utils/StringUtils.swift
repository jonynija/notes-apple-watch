//
//  StringUtils.swift
//  NotesApp WatchKit Extension
//
//  Created by Jontahan Ixcayau on 9/12/23.
//

import Foundation

extension String {
    func trim() -> String {
    return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
   }
}
