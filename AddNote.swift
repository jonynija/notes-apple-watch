//
//  AddNote.swift
//  NotesApp WatchKit Extension
//
//  Created by Jonathan Ixcayau on 27/06/22.
//

import SwiftUI

struct AddNote: View {
    @State private var text = ""
    @State private var notes  = [Note]()
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        VStack{
            TextField(
                "Nota",
                text: $text
            )
            
            Button("Agregar nota"){
                onSaveTap()
            }
        }
        .onAppear(perform: {
            self.notes = Tools.shared.load()
        })
    }
    
    func onSaveTap(){
        guard text.trim().isEmpty == false else {
            return
        }
        
        let note  = Note(title: text)
        notes.append(note)
        
        Tools.shared.save(notes: notes)
        
        text = ""
        presentation.wrappedValue.dismiss()
    }
}

struct AddNote_Previews: PreviewProvider {
    static var previews: some View {
        AddNote()
    }
}
