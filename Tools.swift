//
//  Tools.swift
//  NotesApp WatchKit Extension
//
//  Created by Jonathan Ixcayau on 28/06/22.
//

import Foundation

class Tools {
    
    static let shared = Tools()
    
    private init(){
    }
    
    private let notesKey: String = "NotesKey"
    
    func save(notes: [Note]){
        let data = notes.map{try? JSONEncoder().encode($0)}
        
        UserDefaults.standard.setValue(data, forKey: notesKey)
    }
    
    func load() -> [Note] {
        guard let savedData = UserDefaults.standard.array(forKey: notesKey) as? [Data] else {
            return []
        }
        
        return savedData.map{ try! JSONDecoder().decode(Note.self, from: $0 ) }
    }
    
    func removeNote(note: Note) {
        var savedNotes = self.load()
        
        guard !savedNotes.isEmpty else {
            return
        }
        
        if let noteIndex = savedNotes.firstIndex(of: note) {
            savedNotes.remove(at: noteIndex)
        }
        
        save(notes: savedNotes)
    }
}
