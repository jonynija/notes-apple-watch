//
//  DetailNote.swift
//  NotesApp WatchKit Extension
//
//  Created by Jonathan Ixcayau on 27/06/22.
//

import SwiftUI

struct DetailNote: View {
    let note: Note
    
    var body: some View {
        VStack {
            Text(note.title)
                .font(.system(size: 20))
                .fontWeight(.semibold)
                .foregroundColor(Color.white)
                .multilineTextAlignment(.leading)
            
            Spacer()
            
            Text(note.creationDate)
                .font(.footnote)
                .foregroundColor(Color.white)
                .multilineTextAlignment(.trailing)
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .padding()
    }
}

struct DetailNote_Previews: PreviewProvider {
    static var previews: some View {
        DetailNote(
            note: Note(title: "Recordatorio de comida")
        )
    }
}
