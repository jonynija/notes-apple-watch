//
//  ListNote.swift
//  NotesApp WatchKit Extension
//
//  Created by Jonathan Ixcayau on 27/06/22.
//

import SwiftUI

struct ListNote: View {
    @State var notes : [Note] = []
    
    var body: some View {
        VStack{
            if (notes.isEmpty){
                Text("No hay data para mostrar")
            } else {
                List {
                    ForEach(0..<notes.count, id: \.self){ i in
                        NavigationLink(
                            destination: DetailNote(note: notes[i]),
                            label: {
                                Text("\(notes[i].title)\n\(notes[i].creationDate)")
                                    .lineLimit(2)
                                    .font(.subheadline)
                                    .foregroundColor(Color.white)
                                    .multilineTextAlignment(.leading)
                                    .frame(minWidth: 0, maxWidth: .infinity)
                            }
                        )
                    }
                    .onDelete(perform: delete)
                }
            }
        }
        .onAppear(perform: {
            self.notes = Tools.shared.load()
        })
    }
    
    func delete(offsets: IndexSet){
        withAnimation{
            notes.remove(atOffsets: offsets)
        }
        
        let index = offsets[offsets.startIndex]
        Tools.shared.removeNote(note: notes[index])
    }
}

struct ListNote_Previews: PreviewProvider {
    static var previews: some View {
        ListNote()
            .previewDevice("Apple Watch Series 7 - 41mm")
    }
}
